data Nat = Zero | Suc Nat
  deriving Show

addNat :: Nat -> Nat -> Nat
addNat Zero x = x
addNat (Suc x) y = Suc (addNat x y)

mulNat :: Nat -> Nat -> Nat
mulNat Zero x = Zero
mulNat (Suc x) y = addNat (mulNat x y) y
-- x * y + y

-- mulNat 2 5 = addNat (mulNat 1 5) 5 = addNat (mulNat (addNat (mulNat Zero 5) 5) 5) 5

-- cat :: [a] -> [a] -> [a]
-- cat [] x = x
-- cat (x:xs) ys = x:cat xs ys

