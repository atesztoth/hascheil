module Help where

import Prelude

data Tree a = Leaf1 a | Leaf2 a a | Node (Tree a) (Maybe (Tree a))
  deriving (Eq, Ord, Show)

instance Functor Tree where
  fmap f (Leaf1 l)              = Leaf1 (f l)
  fmap f (Leaf2 l r)            = Leaf2 (f l) (f r)
  fmap f (Node tl Nothing)      = Node (f <$> tl) Nothing
  fmap f (Node tl (Just tr))    = Node (f <$> tl) (Just (f <$> tr))

instance Foldable Tree where
  foldr f x (Leaf1 l)             = f l x
  foldr f x (Leaf2 a1 a2)         = f a2 (f a1 x)
  foldr f x (Node tl Nothing)     = foldr f x tl
  foldr f x (Node tl (Just tr))   = foldr f (foldr f x tl) tr

instance Traversable Tree where
  traverse f (Leaf1 l)            = Leaf1 <$> f l
  traverse f (Leaf2 l r)          = Leaf2 <$> f l <*> f r
  traverse f (Node tl Nothing)    = (\tl -> Node tl Nothing) <$> traverse f tl -- <-- megvan, megvan... Istenem hát ez nagyon idegesítő volt.
  traverse f (Node tl (Just tr))  = Node <$> traverse f tl <*> (Just <$> traverse f tr)