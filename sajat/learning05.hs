module Learning05 where

import qualified Data.Map as Map

data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday   
    deriving (Eq, Ord, Show, Read, Bounded, Enum)

data LockerState = Free | Taken deriving (Eq, Show)

type Code = String -- type alias

type LockerMap = Map.Map Int (LockerState, Code)

lockers :: LockerMap  
lockers = Map.fromList   
    [(100,(Taken,"ZD39I"))  
    ,(101,(Free,"JAH3I"))  
    ,(103,(Free,"IQSA9"))  
    ,(105,(Free,"QOTSA"))  
    ,(109,(Taken,"893JJ"))  
    ,(110,(Taken,"99292"))  
    ]  

-- check if locker is already taken
lookupLocker :: Int -> LockerMap -> Either String Code
lookupLocker n map =
    case Map.lookup n map of
        Nothing -> Left $ "This locker " ++ show n ++ " doesn't exist!"
        Just (state, code) -> if state /= Taken then Right code else Left "already taken. :/"


data BinTree a = EmptyTree | Node a (BinTree a) (BinTree a) deriving (Show)

singleton :: a -> BinTree a
singleton x = Node x EmptyTree EmptyTree

threeTree :: BinTree Int
threeTree = Node 3 (Node 2 (Node 1 EmptyTree EmptyTree) EmptyTree) EmptyTree

treeInsert :: (Ord a) => a -> BinTree a -> BinTree a
treeInsert x EmptyTree = singleton x
treeInsert x (Node a left right)
    | x == a = Node x left right
    | x < a  = Node a (treeInsert x left) right
    | x > a  = Node a left (treeInsert x right)

elemInTree :: (Ord a) => a -> BinTree a -> Bool
elemInTree _ EmptyTree = False
elemInTree x (Node val left right)
    | x == val = True
    | x < val = elemInTree x left
    | x > val = elemInTree x right