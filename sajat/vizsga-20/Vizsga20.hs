module Vizsga20 where

data XTree a = Omega | Alpha a (XTree a) | Beta a (XTree a) (XTree a)
  deriving (Eq, Ord, Show)


ex1 :: XTree Int
ex1 = Alpha 1 $ Alpha 2 $
        Beta 3
          (Alpha 4 Omega)
          (Alpha 5 $ Alpha 6 $
            Beta 7
              (Alpha 8 Omega)
              Omega
          )


instance Functor XTree where
  -- fmap :: (a -> b) -> f a -> f b
  fmap _ Omega = Omega
  fmap f (Alpha x t) = Alpha (f x) (f <$> t)
  fmap f (Beta x l r) = Beta (f x) (f <$> l) (f <$> r)

instance Foldable XTree where
  -- foldr :: (a -> b -> b) -> b -> t a -> b
  foldr _ i Omega = i
  foldr f i (Alpha x t) = f x (foldr f i t)
  foldr f i (Beta x l r) = f x (foldr f (foldr f i r) l)

instance Traversable XTree where
    -- traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
    traverse _ Omega = pure Omega
    traverse f (Alpha a t) = Alpha <$> f a <*> traverse f t
    traverse f (Beta x l r) = Beta <$> f x <*> traverse f l <*> traverse f r

leastElem :: Ord a => XTree a -> a
leastElem t = minimum t

-- logic: see if an element exists in a tree by folding the tree into a simple logical value
-- by comparing every element with the current one
-- Do this for all elements in the list
containsAll :: Eq a => XTree a -> [a] -> Bool
containsAll t l = foldl (\acc -> \curr -> contains t curr && acc) True l where
  contains :: Eq a => XTree a -> a -> Bool
  contains t' a' = foldl (\acc -> \curr -> curr == a' || acc) False t'


-- you are next:
-- Mélység szerinti címkézés (2 pont)

-- Adjuk meg azt a függvényt, amely egy XTree minden csúcsát megcímkéz a mélységével!

labelDepth :: XTree a -> XTree (a, Int)
labelDepth = undefined

