module Learning04 where

import Prelude hiding (maximum)
import Data.List

-- lets define a maximum finder function
maximum' :: (Ord a) => [a] -> a
maximum' [] = error "Empty list. It has no max"
maximum' [x] = x
maximum' (x:xs)
    | x > maxTail = x
    | otherwise = maxTail
    where maxTail = maximum' xs

-- lazyness in action!
largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [100000, 99999..])
  where p x = x `mod` 1234 == 0


numUniques :: (Eq a) => [a] -> Int
numUniques = length . nub

-- yep, dont forget about that you can give explicit type names too
data Circle = Circle Float Float Float deriving (Show)

myCircle = Circle 1.0 2.0 3.5

-- The record syntax in an understadble form
-- data Person = Person String String Int Float String String deriving (Show)  
-- is eq
-- (!important info: flavour is the flavour of one's fav. ice cream lol)
-- data Person = Person { firstName :: String  
--                      , lastName :: String  
--                      , age :: Int  
--                      , height :: Float  
--                      , phoneNumber :: String  
--                      , flavor :: String  
--                      } deriving (Show) 

-- There are some "minor" things that if only we heard at the practice would made
-- at least my life a lot easier.
-- "The resulting data type is exactly the same. 
-- The main benefit of this is that it creates functions that
-- lookup fields in the data type. By using record syntax to 
-- create this data type, Haskell automatically made these 
-- functions: firstName, lastName, age, height, phoneNumber 
-- and flavor" - Sándor Petőfi. xd ok not.

-- so lets just:
-- guy = Person "Buddy" "Finklestein" 43 184.2 "526-2928" "Chocolate" 
-- firstName guy => "Buddy" . Awesome!

data Person2 = Person { firstName :: String  
                     , lastName :: String  
                     , age :: Int  
                     } deriving (Eq, Show, Read)  

-- very good and you can do stuff like
-- read "Person { firstName=\"Michael\" lastName=\"De Santa\" age=51" :: Person

