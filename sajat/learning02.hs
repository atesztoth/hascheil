module Learning02 where

eqList :: (a -> a -> Bool) -> [a] -> [a] -> Bool
eqList f [] [] = True
eqList f (x:xs) (y:ys) = f x y && eqList f xs ys
eqList _ _ _ = False

eqBool :: Bool -> Bool -> Bool
eqBool True True = True
eqBool False False = True
eqBool _ _ = False

-- példák:
eqListBool :: [Bool] -> [Bool] -> Bool
eqListBool = eqList eqBool

eqListListBool :: [[Bool]] -> [[Bool]] -> Bool
eqListListBool = eqList (eqList eqBool)

bigEq :: [[(Bool, [Bool])]] -> [[(Bool, [Bool])]] -> Bool
bigEq = eqList (eqList (eqPair eqBool (eqList eqBool))) -- brutal composition here? oh m8

-- kis megértésképp hogy írjak már valami saját típust is, meg arra mondjuk egy típusosztályt,
-- írjunk egy egyenlőséget egy saját típusra

data Direction = L | R | U | D deriving Show

class Eq' a where
  (==) :: a -> a -> Bool

instance Eq' Direction where
  L == L = True
  R == R = True
  U == U = True
  D == D = True
  _ == _ = False

-- just to practice, to get used to things
eqPair :: (a -> a -> Bool) -> (b -> b -> Bool) -> (a, b) -> (a, b) -> Bool
eqPair f g p1 p2 = f (fst p1) (fst p2) && g (snd p1) (snd p2)

data Point = P Int Int

instance Eq' Point where
  (==) (P a b) (P x y) = a Prelude.== x && b Prelude.== y

data Nat = Zero | Succ Nat

instance Eq' Nat where
  Zero == Zero = True
  Succ n == Succ m = n Learning02.== m
  _ == _ = False

-- yo, lets put the dot onto the ... map !

drMap :: (a -> b) -> [a] -> [b]
drMap f [] = []
drMap f (x:xs) = f x : drMap f xs

-- kompozíció

f1 :: (b -> c) -> (a -> b) -> a -> c
-- f1 f g a = _
-- lambda form, mert looks vagányabb because its more mathematical
f1 = \f g a -> f (g a)