{-# LANGUAGE InstanceSigs #-}

module GVizsga13 where

import Control.Applicative
-- import Control.Monad.State
import Control.Monad.Trans
import Control.Monad.Identity
import Control.Monad.Error.Class
import Control.Monad.Trans.State
import Control.Monad.Trans.Except
import Data.Map (Map(..))
import Data.Char
import Data.Functor (void)
import qualified Data.Map as Map

data Either' a b = Left' a | Right' b | Both a b
  deriving (Eq, Show)

instance Functor (Either' a) where
  fmap g (Right' x) = Right' (g x)
  fmap g (Both fix b) = Both fix (g b)
  fmap _ (Left' x) = (Left' x)

instance Foldable (Either' a) where
  foldr f init (Left' x) = init
  foldr f init (Right' x) = f x init
  foldr f init (Both _ x) = f x init

instance Traversable (Either' a) where
  traverse _ (Left' x) = pure (Left' x)
  traverse f (Right' x) = Right' <$> f x
  traverse f (Both a x) = (Both a) <$> f x

type MState a b = ([a], [b], [(a, b)])

helper :: Either' a b -> MState a b
helper (Left' x) = ([x], [], [])
helper (Right' x) = ([], [x], [])
helper (Both x y) = ([], [], [(x, y)])

monadicCollect :: [Either' a b] -> State ([a], [b], [(a, b)]) [Either' a b]
monadicCollect [] = pure []
monadicCollect (x:xs) = do
  c <- get
  put (c `mappend` (helper x))
  monadicCollect xs

partition :: [Either' a b] -> ([a], [b], [(a, b)])
partition [] = ([], [], [])
partition xs = snd (runState (monadicCollect xs) ([], [], []))

zipWith' :: (Either' a b -> c) -> [a] -> [b] -> [c]
zipWith' f [] [] = []
zipWith' f (x:xs) (y:ys) = [f (Both x y)] ++ (zipWith' f xs ys)
zipWith' f (x:xs) [] = [f (Left' x)] ++ zipWith' f xs []
zipWith' f [] (y:ys) = [f (Right' y)] ++ zipWith' f [] ys

mapMaybeLeft :: (a -> Maybe b) -> [Either' a c] -> Maybe [Either' b c]
mapMaybeLeft f [] = pure []
mapMaybeLeft f ((Left' x):xs) = do
  res <- f x
  rest <- mapMaybeLeft f xs
  return ((:) (Left' res) rest)
mapMaybeLeft f ((Both x y):xs) = do
  res <- f x
  rest <- mapMaybeLeft f xs
  return ((:) (Both res y) rest)
mapMaybeLeft f ((Right' x):xs) = do
  rest <- mapMaybeLeft f xs
  return ((:) (Right' x) rest)

data Tree a = Leaf a | Node (Tree a) (Tree a)
  deriving (Eq, Show)

instance Functor Tree where
    fmap f (Leaf x) = Leaf (f x)
    fmap f (Node l r) = Node (fmap f l) (fmap f r)

instance Foldable Tree where
    foldr f v0 (Leaf x) = f x v0
    foldr f v0 (Node l r) = foldr f (foldr f v0 l) r

instance Traversable Tree where
    traverse f (Leaf x) = Leaf <$> (f x)
    traverse f (Node l r) = Node <$> (traverse f l) <*> (traverse f r)

type StateType = Int
type ComputationType = Tree Int
treeSummer :: Tree Int -> State StateType ComputationType
treeSummer (Node l r) = do
  leftTree <- treeSummer l
  put (foldl (+) 0 l)
  rightTree <- treeSummer r
  return (Node leftTree rightTree)
treeSummer (Leaf x) = do
  n <- get
  return (Leaf n)

treeSums :: Tree Int -> Tree Int
treeSums t = evalState (treeSummer t) 0

--- While nyelv (part 2)

-- SYNTAX

data Lit
  = LBool Bool
  | LInt Int
  deriving (Eq, Ord, Show)

type Name = String

newtype Var = Var Name
  deriving (Eq, Ord, Show)

data Expr
  -- atoms
  = ELit Lit
  | EVar Var
  -- arithmetic
  | Plus Expr Expr
  | Minus Expr Expr
  | Mul Expr Expr
  -- logical
  | And Expr Expr
  | Eq Expr Expr
  | LEq Expr Expr
  | Not Expr
  deriving (Eq, Ord, Show)

data Statement
  = Skip
  | Seq Statement Statement
  | If Expr Statement Statement
  | While Expr Statement
  | Assign Var Expr
  | LogStr String
  | LogInt Expr
  deriving (Eq, Ord, Show)


newtype RTVal = RTLit Lit
  deriving (Eq, Ord, Show)

type VarMapping = Map Var RTVal

type Eval a = StateT VarMapping (ExceptT String Identity) a

runEval :: Eval a -> VarMapping -> Either String (a, VarMapping)
runEval m s = runExcept (runStateT m s)

evalEval :: Eval a -> VarMapping -> Either String a
evalEval m s = fst <$> runEval m s

evalLit :: Lit -> Eval RTVal
evalLit lit = return $ RTLit lit

evalVar :: Var -> Eval RTVal
evalVar v = do
  vars <- get
  let mVal = Map.lookup v vars
  case mVal of
    Just val -> return val
    Nothing  -> throwError $ "Undefined variable: " ++ show v

evalBinOp :: (Expr -> Eval a) ->
             (Expr -> Eval b) ->
             (c -> RTVal) ->
             (a -> b -> c) ->
             (Expr -> Expr -> Eval RTVal)
evalBinOp evalLhs evalRhs mkRetVal op lhs rhs = do
  lhs' <- evalLhs lhs
  rhs' <- evalRhs rhs
  let result = lhs' `op` rhs'
  return $ mkRetVal result

evalInt :: Expr -> Eval Int
evalInt e = do
  e' <- evalExpr e
  case e' of
    RTLit (LInt n) -> return n
    _ -> throwError $ show e ++ " does not evaluate to an Integer"

evalBool :: Expr -> Eval Bool
evalBool e = do
  e' <- evalExpr e
  case e' of
    RTLit (LBool b) -> return b
    _ -> throwError $ show e ++ " does not evaluate to a Boolean"

mkRTInt :: Int -> RTVal
mkRTInt = RTLit . LInt

mkRTBool :: Bool -> RTVal
mkRTBool = RTLit . LBool

evalUnaryOp :: (Expr -> Eval a) ->
               (b -> RTVal) ->
               (a -> b) ->
               (Expr -> Eval RTVal)
evalUnaryOp evalArg mkRetVal op arg =
  evalBinOp evalArg evalArg mkRetVal (const <$> op) arg arg
  -- const <$> op is similar to: \lhs rhs -> op lhs

evalExpr :: Expr -> Eval RTVal
evalExpr (ELit l)        = evalLit l
evalExpr (EVar v)        = evalVar v
evalExpr (Plus lhs rhs)  = evalBinOp evalInt evalInt mkRTInt (+) lhs rhs
evalExpr (Minus lhs rhs) = evalBinOp evalInt evalInt mkRTInt (-) lhs rhs
evalExpr (Mul lhs rhs)   = evalBinOp evalInt evalInt mkRTInt (*) lhs rhs
evalExpr (And lhs rhs)   = evalBinOp evalBool evalBool mkRTBool (&&) lhs rhs
evalExpr (LEq lhs rhs)   = evalBinOp evalInt evalInt mkRTBool (<=) lhs rhs
evalExpr (Not arg)       = evalUnaryOp evalBool mkRTBool (not) arg
evalExpr (Eq l r)        = evalBinOp evalInt evalInt mkRTBool (==) l r

evalStatement :: Statement -> Eval ()
evalStatement s = case s of
  Skip      -> pure ()
  Seq s s'  -> evalStatement s >> evalStatement s'
  If e s s' -> do
    b <- evalBool e
    if b then evalStatement s
         else evalStatement s'
  While e s -> do
    b <- evalBool e
    if b then evalStatement s >> evalStatement (While e s)
         else pure ()
  Assign x e -> do
    v <- evalExpr e
    modify $ Map.insert x v


type Parser a = StateT String Maybe a

runParser :: Parser a -> String -> Maybe (a, String)
runParser = runStateT

evalParser :: Parser a -> String -> Maybe a
evalParser p s = fmap fst $ runParser p s

eof :: Parser ()
eof = do
  str <- get
  case str of
    "" -> pure ()
    _  -> empty

satisfy :: (Char -> Bool) -> Parser Char
satisfy f = do
  str <- get
  case str of
    c:cs | f c -> c <$ put cs
    _          -> empty

char :: Char -> Parser Char
char c = satisfy (== c)

lowerAlpha :: Parser Char
lowerAlpha = satisfy isLower

natural :: Parser Int
natural = read <$> some (satisfy isDigit)

token :: Parser a -> Parser a
token pa = pa <* ws

string :: String -> Parser ()
string s = () <$ traverse char s

string' :: String -> Parser ()
string' s = token (string s)

ws :: Parser ()
ws = () <$ many (satisfy isSpace)

iLit :: Parser Lit
iLit = LInt <$> token natural

bLit :: Parser Lit
bLit =  (LBool True  <$ string' "true")
    <|> (LBool False <$ string' "false")

lit :: Parser Lit
lit = iLit <|> bLit

-- sLit
-- parse string between "" that is not any other idézőjel
sLit :: Parser String -- s🔥
sLit = char '\"' *> many (satisfy (/='\"')) <* string' "\""

parens :: Parser a -> Parser a
parens p = string' "(" *> p <* string' ")"

keywords :: [String]
keywords = ["Skip", "If", "then", "else", "While", "do", "end", "true", "false", "not"]

ident :: Parser String
ident = do
  x <- token (some lowerAlpha)
  if elem x keywords
    then empty
    else pure x

var :: Parser Var
var = Var <$> ident

-- "3+5" -> EPlus (ELit (Lit 3)) (ELit (Lit 5))
expr' :: Parser Expr
expr' = (ELit <$> lit)
    <|> (EVar <$> var)
    <|> parens expr

expr :: Parser Expr
expr =
        (Not <$> (string' "not" *> expr))
    <|> Plus  <$> expr' <*> (string' "+"  *> expr)
    <|> Minus <$> expr' <*> (string' "-"  *> expr)
    <|> Mul   <$> expr' <*> (string' "*"  *> expr)
    <|> And   <$> expr' <*> (string' "&&" *> expr)
    <|> Eq    <$> expr' <*> (string' "==" *> expr)
    <|> LEq   <$> expr' <*> (string' "<=" *> expr)
    <|> expr'

statement' :: Parser Statement
statement' = (string' "Skip" *> pure Skip)
        <|> Assign <$> var <*> (string' ":=" *> expr)
        <|> If <$> (string' "If"   *> expr)
               <*> (string' "then" *> statement)
               <*> (string' "else" *> statement)
        <|> While <$> (string' "While" *> expr)
                  <*> (string' "do"    *> statement <* string' "end")

statement :: Parser Statement
statement = Seq <$> statement' <*> (string' ";" *> statement)
        <|> statement'
