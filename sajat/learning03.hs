module Learning03 where

import Prelude hiding (Monoid(..), Semigroup(..), Functor(..))

-- Semigroup (félcsoport)
-- (S;*) egyműveletes struktrúa, melyben a binér * művelet asszociatív.

class Semigroup m where
  -- név: mappend
  (<>) :: m -> m -> m

newtype Sum a = Sum { getSum :: a }
  deriving (Show, Eq, Ord)

newtype Prod a = Prod { getProd :: a }
  deriving (Show, Eq, Ord)

instance Semigroup (Sum Int) where
  (<>) (Sum n) (Sum k) = Sum (n + k)

-- monoidnak van semleges eleme
class Semigroup m => Monoid m where
  mempty :: m

instance Monoid (Sum Int) where
  mempty = Sum 0

instance Monoid (Prod Int) where
  mempty = Prod 1

data List a = Nil | Cons a (List a) deriving (Show, Eq, Ord)

class Functor (f :: * -> *) where
  fmap :: (a -> b) -> f a -> f b

instance Functor List where
  fmap :: (a -> b) -> List a -> List b
  fmap f Nil = Nil
  fmap f (Cons x xs) = Const (f x) (fmap f xs)
