module StateMonad where

import Control.Monad.State

type Stack = [Int]

pop :: Stack -> (Int, Stack)
pop (x:xs) = (x, xs)

push :: Int -> Stack -> ((), Stack)
push a xs = ((), a:xs)

-- now yeah, this is kinda a stateful thing. Stack is our state!
-- from  this, lets start moving towards state monads

-- lets just use it...
stackManip :: Stack -> (Int, Stack)
stackManip stack = let
    ((), newStack1) = push 3 stack
    (a , newStack2) = pop newStack1
    in pop newStack2

-- yeah just pushing popping elements, great, works!
-- and actually looks freaking cool

-- experimenting:
-- manipulatedStack :: (Int, Stack)
-- manipulatedStack = stackManip [1..10]

-- ookay, instead of manually passing around the state, lets introduce
-- the man himself: State monad!

-- all righthy, let's look at the definitions:
-- from control.monad.state:     newtype State s a = State { runState :: s -> (a,s) }
-- instance Monad (State s) where
  -- return x = State $ \s -> (x,s)
  -- (State h) >>= f = State $ \s -> let (a, newState) = h s
  --                                     (State g) = f a
  --                                 in  g newState

-- lets make this Stack thing a state monad!
-- These wont work, it wants me to use a StateT monad transformer, but cannot initiate it yet.
-- pop' :: State Stack Int
-- pop' = StateT $ \(x:xs) -> (x, xs)

-- push' :: Int -> State Stack ()
-- push' a = StateT $ \xs -> ((), a:xs)

-- -- Yeah lets do it by only defining the type and let haskell handle things
-- stackManIp :: State Stack Int
-- stackManIp = do
--   push 3
--   a <- pop
--   pop
