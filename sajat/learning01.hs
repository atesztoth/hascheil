factorial :: (Integral a) => a -> a
factorial 0 = 1 -- itt egy pattern match
factorial x = x * factorial(x-1) -- itt meg egy rekurzió

thrd :: (a, b, c) -> c
thrd (x, z, y) = y

head' :: [a] -> a
head' [] = error "Cannot get the head of an empty list!"
head' (x:xs) = x

listOkoskodo :: (Show a) => [a] -> String
listOkoskodo [] = "The list is empty"
listOkoskodo (x:[]) = "The list has only one element: " ++ show x -- Syntactic sugar would work: [x]
listOkoskodo (x:y:[]) = "The list has two elements. 1st: " ++ show x ++ " 2nd: " ++ show y -- Syn. sugar: [x,y]
listOkoskodo (x:y:_) = "The list 3 or more elems. Show first 2. 1st: " ++ show x ++ " 2nd: " ++ show y

-- getting into recursion
length' :: (Num a) => [a] -> a
length' [] = 0
length' (x:xs) = 1 + length' xs

-- writing sum
sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

-- listOkoskodo megtuningolva egy kis patternnel 👨🏼‍🍳
listOkoskodo' :: (Show a) => [a] -> String
listOkoskodo' [] = "The list is empty"
listOkoskodo' (x:[]) = "The list has only one element: " ++ show x -- Syntactic sugar would work: [x]
listOkoskodo' (x:y:[]) = "The list has two elements. 1st: " ++ show x ++ " 2nd: " ++ show y -- Syn. sugar: [x,y]
listOkoskodo' xs@(x:y:_) = "The list 3 or more elems. Showing all: " ++ show xs

-- ## Guards
-- (just a quick look)

guardExample :: (Num a, Ord a) => a -> String
guardExample number
  | number < 0               = "You are so negative :("
  | number > 0 && number < 5 = "Yeah you're getting to the first real example"
  | otherwise                = "Whats good? Type a more little number!"

guard2 :: (Num a, Ord a) => a -> a -> String
-- guard2 a b
--   | sum < first     = "You are so negative :("
--   | sum < second    = "Yeah you're getting to the first real example"
--   | otherwise       = "Whats good? Type a more little number!"
--   where sum = a + b
--         first = 0
--         second = 10

guard2 a b
  | sum < first     = "You are so negative :("
  | sum < second    = "Yeah you're getting to the first real example"
  | otherwise       = "Whats good? Type a more little number!"
  where (sum, first, second) = (a + b, 0, 10)

-- Let's write a typeclasses!

class WeirdEq a where
  weq :: a -> a -> Bool

instance WeirdEq Bool where
  weq True _ = True
  weq _ _ = False

instance WeirdEq Int where
  weq a b = a == (b - 1)