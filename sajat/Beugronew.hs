module Beugronew where

import Control.Monad.State

-- ami a trükkös ebben, hogy az állapotban összegezzük az elemszámot
-- vagyis üres listára egy olyan Stateful számítással kell visszatérjünk, ami
-- egy kezdeti állapot mellett nullát tárol
-- ez valami ilyesmit jelent: [1,2,3] -> State $ \s -> ((), 0)
-- tehát nem térünk vissza semmilyen értékkel, marad Unit, de a state 0 lesz
-- returnt itt így 0-val nem használhatunk, hiszen az pontosan az értéket állítaná át.
-- return x = State $ \s -> (x,s) === fogja az értékünket, és mellétesz egy akármiylen statet használatnál
lengthM :: [a] -> State Int ()
lengthM [] = return () -- ennek az eredménye State $ \s -> ((), mempty) -- mempty AKA 0 az Int-nél
lengthM (x:xs) = do
  storedLength <- get
  put (storedLength + 1)
  lengthM xs

-- usage:
-- Talán a legegyszerűbb úgy használi, ha mint "monad factory"-ra gondolunk. Mert hát monadot csinál...
-- Gyártsuk le a monadot az adott listánkat felhasználva. Tehát a létrejövő monad
-- amolyan függvénye lesz a beadott listának, és maga a rajta lévő számítás lesz monadikus.
-- tehát:
createdMonad = lengthM [1..150]
-- ez a monad még vár egy kezdeti state-re. Ezt megadjuk mikor számítjuk a state-t:
(res, count) = runState createdMonad 0
-- az eredmenyek a res-ben (unit lesz :O), és a state pedig a countban!

-- 2
-- itt ugye a lényeg abban van, hogy (>>=) :: m a -> a -> m b -> m b
-- mivel dupla monad van, ha a belső függvény mondjuk véletlenül visszaadná a belső monadikus értéket...
-- akkor itt bizony a külső leesne. A külső szempontjából ugyanis annyi történik, hogy
-- ahelyett, hogy folytatná az ő (második szintű) monadikus chainjét ez a beadott függvény,
-- egy szinttel lejjebb dobja a monadokat, mivel a külső szinttel már nem tér vissza...
-- Na jó ez valahogy értelmesebben: van egy monad egy monadban. Adsz neki egy fv-t, amit ő
-- alkalmazni fog a belső értékére. Na most a külső monadnak a belsejében egy másik monad van.
-- Ha ezt odaadod az fv-nek, az már csak a belsőt fogja visszadni... (id fv).
-- az fv kimeneti elvárt típusa pedig pontosan ez.
reduce :: Monad m => m (m a) -> m a
reduce m = m >>= id

pl = reduce (Just (Just 5))
