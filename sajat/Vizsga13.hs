module Vizsga13 where

data Either' a b = Left' a | Right' b | Both a b
  deriving (Eq, Show)

-- csak a b paramra fogunk fmapet írni, mivel ugye a functor egy típus konstruktort vár be. AKA egy fv-t
-- (Az Either' a egy egy paramteres fv. (curry))
instance Functor (Either' a) where
  fmap f (Right' x) = Right' (f x)
  fmap f (Both x y) = Both x (f y)
  fmap _ (Left' x) = Left' x

instance Foldable (Either' a) where
  foldr f v (Left' a) = v
  foldr f v (Right' x) = f x v
  foldr f v (Both _ x) = f x v

instance Traversable (Either' a) where
  -- traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
    traverse f (Right' x) = Right' <$> (f x)
    traverse f (Both x y) = (Both x) <$> (f y)
    traverse f (Left' v) = pure (Left' v)

partition :: [Either' a b] -> ([a], [b], [(a, b)])
partition [] = ([], [], [])
partition xs = collectElems ([], [], []) xs where
  collectElems :: ([a], [b], [(a, b)]) -> [Either' a b]-> ([a], [b], [(a, b)])
  collectElems (k, j, l) ((Left' x'):xs) = let (k', j', l') = collectElems (k ++ [x'], j, l) xs in (k', j', l')
  collectElems (k, j, l) ((Right' y'):xs) = let (k', j', l') = collectElems (k, j ++ [y'], l) xs in (k', j', l')
  collectElems (k, j, l) ((Both x'' y''):xs) = let (k', j', l') = collectElems (k, j , l ++ [(x'', y'')]) xs in (k', j', l')
  collectElems (k, j, l) _ = (k, j, l)

zipWith' :: (Either' a b -> c) -> [a] -> [b] -> [c]
zipWith' f [] [] = []
zipWith' f (x:xs) (y:ys) = [f (Both x y)] ++ (zipWith' f xs ys)
zipWith' f (x:xs) [] = [f (Left' x)] ++ zipWith' f xs []
zipWith' f [] (y:ys) = [f (Right' y)] ++ zipWith' f [] ys

-- Alkalmazzunk egy a -> Maybe b függvényt minden a típusú értéken az inputban.
-- Ha bármelyik függvényhívás eredménye Nothing, legyen a végeredmény Nothing,
-- egyébként a végeredmény Just-ban az output lista, ahol a függvényt minden a-ra alkalmaztuk.
-- Tipp: használjunk Maybe monádot. (2 pont)

mapMaybeLeft :: (a -> Maybe b) -> [Either' a c] -> Maybe [Either' b c]
-- mapMaybeLeft f [] = Nothing
-- mapMaybeLeft f xs = sequence f xs
mapMaybeLeft = undefined

-- Példák:

-- mapMaybeLeft (\_ -> Just True) [Left' ()] == Just [Left' True]
-- mapMaybeLeft (\x -> if x == 0 then Nothing else Just x) [Right' 10, Both 0 10] == Nothing
-- mapMaybeLeft (\x -> if x == 0 then Nothing else Just x) [Right' 10, Both 1 10] == Just [Right' 10,Both 1 10]

data Tree a = Leaf a | Node (Tree a) (Tree a)
  deriving (Eq, Show)

instance Functor Tree where
  fmap f (Leaf x) = Leaf (f x)
  fmap f (Node l r) = Node (fmap f l) (fmap f r)

-- treeSums :: Tree Int -> Tree Int
-- treeSums (Leaf a) = 0
-- treeSums (Node l r) = Node (annotate l 0) (annotate r 0) where
--   annotate :: Tree Int -> Int -> (n, Tree Int)
--   annotate (Leaf x) n = (x + n, Leaf n)
--   annotate (Node l r) n = Node (annotate l n) (annotate r n)
