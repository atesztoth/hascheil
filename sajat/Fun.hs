module Fun where

type Birds = Int
type Pole = (Birds, Birds)

landLeft :: Birds -> Pole -> Maybe Pole
landLeft n (left, right)
  | abs ((newLeft) - right) < 4 = Just (newLeft, right)
  | otherwise                    = Nothing
    where newLeft = left + n

landRight :: Birds -> Pole -> Maybe Pole
landRight n (left, right)
  | abs (left - (newRight)) < 4 = Just (left, newRight)
  | otherwise                    = Nothing
    where newRight = right + n

-- nice util
x -: f = f x

-- then we can do things like:
tryit :: Maybe Pole
tryit = return (0,0) >>= landLeft 1 >>= landRight 1 >>= landLeft (-1) >>= landRight 1

-- na jo jo kis do notationt -- yep, chaineli nekünk itt szépen a monadokat a binddel
routine :: Maybe Pole
routine = do
  start <- return (0,0)
  first <- landLeft 2 start
  second <- landRight 2 first
  landLeft 1 second

-- eq:
-- routine :: Maybe Pole
-- routine =
--     case Just (0,0) of
--         Nothing -> Nothing
--         Just start -> case landLeft 2 start of
--             Nothing -> Nothing
--             Just first -> case landRight 2 first of
--                 Nothing -> Nothing
--                 Just second -> landLeft 1 second

failedRoutine :: Maybe Pole
failedRoutine = do
    start <- return (0,0)
    first <- landLeft 2 start
    Nothing -- chainelés közben beesik egy Nothing... whops
    second <- landRight 2 first
    landLeft 1 second
