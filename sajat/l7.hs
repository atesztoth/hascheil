module Own07 where

import Prelude hiding (Functor(..))

class (Functor f) where
  fmap :: (a -> b) -> f a -> f b

instance Functor ((->) r) where
  fmap f g = (\x -> f (g x))
