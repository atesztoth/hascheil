{-# LANGUAGE InstanceSigs, KindSignatures #-}

module Learning06 where

import Prelude hiding (Functor(..), Either(..))

-- let's get applicative!!

class Functor (f :: * -> *) where
  fmap :: (a -> b) -> f a -> f b

data MyEither a = Left a | Right a deriving Show

instance Functor MyEither where
  fmap f (Left a) = Left (f a)
  fmap f (Right a) = Right (f a)

-- oh I recetly I started loving these infinite types
-- lets create an infinite binary tree

data Bintree a = Leaf | Node a (Bintree a) (Bintree a) deriving Show

sampleTree :: Bintree Int
sampleTree = Node 1 (Node 2 Leaf Leaf) (Node 3 Leaf Leaf)

data InfiniTree k v
  = Nil
  | Branch v (k -> InfiniTree v k)

sampleInfinitree :: InfiniTree Bool Int
sampleInfinitree = Branch 1 (\_ -> Nil)
