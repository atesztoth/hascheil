module Practice3 where

import Prelude hiding (Maybe(..))

newtype Fun a b = Fun (a -> b)

-- $ operator az olyan cucc (függvényalkalmazás) hogy ami jobbra van tőle, az olyan, mintha zárójelezve lenne
-- miért? Mert függvényalkalmazás!!
-- pl: függvény 3 paraméterrel: f:: int -> int
-- ezt mondjuk alkalmaznánk dolgokra: f(f(f(f(1)))) = f$f$f$f$1 // lehet hogy eltévesztettem, lényeg hogy olvasható
-- "ennek a legjobb bartája a függénykomp.": (.) f g -> f(gx)
-- ez így nagyon menőbe: f.f.f$1 // ez kb úgy megy ,hogy megkonstruáljuk kompozícióval, hogy mely függvényt akarjuk alkalmazni,
-- aztán a végén beadunk neki egy adatot a dollárjellel
instance Semigroup b => Semigroup (Fun a b) where
  (<>) (Fun f) (Fun g) = Fun $ \x -> f x <> g x

instance Monoid b => Monoid (Fun a b) where
  mempty = Fun $ \x -> mempty

data Maybe a = Nothing | Just a
  deriving (Eq, Ord, Show)

instance Functor Maybe where
  fmap _ Nothing = Nothing
  fmap f (Just x) = Just (f x)

instance Functor (Fun a) where
  fmap f (Fun fun) = Fun $ \x -> f (fun x)

-- RoseTree (egy tree mely véges sok irányba tud elágazni minden csomópontban)
-- data RoseTree a
--   = Nil
--   | Branch [RoseTree a]

data InfiniTree k v
  = Nil
  | Branch v (k -> InfiniTree v k)

-- type MaybeTree a = InfiniTree _ _

-- type ListTree a = InfiniTree _ _

type BinTree a = InfiniTree Bool a

-- ez nem typecheckel
binTree :: BinTree Int
binTree = Branch 1 (\x -> if x then Nil else Branch 2 (const Nil))

-- type RoseTree a = InfiniTree _ _
