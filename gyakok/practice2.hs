{-# LANGUAGE FlexibleInstances, KindSignatures, InstanceSigs #-}

module Practice2 where

import Prelude hiding (Semigroup(..), Monoid(..), Functor(..))-- az összes előredefiniált dolgot tartalmazó modul

fact :: Int -> Int
fact 0 = 1 -- mintaillesztés
fact n = n * fact (n - 1)

data List a = Nil | Cons a (List a)
  deriving (Eq, Ord, Show) -- deriving: automatikusan példányosítsa a típusosztályt a megadott típusokra: kódgenerálás!

len :: List a -> Int
len Nil = 0
len (Cons _ xs) = 1 + len xs -- pattern matching van itt is

-- Félcsoport (Semigroup)
-- Tud egy műveletet, ami asszociatív, ennyi.

class Semigroup (m :: *) where
  -- mappend
  (<>) :: m -> m -> m

-- data: teszőleges számú adatkonstruktor az tetszőleges mezővel
-- newtype csak 1 adatkonstruktor 1 mezővel (futtatási időben eltűnik, nincs runtime overhead!)

-- newtype Sum a = Sum a
--   deriving (Eq, Ord, Show)

-- getSum :: Sum a -> a
-- getSum (Sum x) = x

-- eqvivalens a lentivel

newtype Sum a = Sum { getSum :: a } -- record syntax
  deriving (Eq, Ord, Show)

newtype Prod a = Prod { getProd :: a }
  deriving (Eq, Ord, Show)

-- nem tudjuk az intre h + vagy * legyen
instance Semigroup (Sum Int) where
  (<>) (Sum n) (Sum k) = Sum (n + k)

instance Semigroup (Prod Int) where
  (<>) (Prod n) (Prod k) = Prod (n * k)

-- monoid: Egységelemes félcsop.
class Semigroup m => Monoid m where -- semigroup legyen superclassa a Monoidnak, AKA bármely típusra példányosítom, arra kell h legyen a semigroup is
  mempty :: m

instance Monoid (Sum Int) where
  mempty = Sum 0 -- egyésgelem összeadásra

instance Monoid (Prod Int) where
  mempty = Prod 1 -- egységelem szorzásra

-- hf:
-- List kifejezése algebrailag
-- Semigroup, Monoid Listre
-- Találni olyan típust, amely Semigroup, de nem Monoid. Akár mi is konstruálhatunk

-- hmm Hf try:
data MyList a = Empty | Elem a (MyList a)
  deriving(Eq, Show)

-- todo: finish / continue

-- Funktorok
-- Functor f :: * -> * -- F kindja * -> *
-- Kind ~ type of types -- típusok típusa pl hány paramot vár. Pl: Int :
-- 5 :: Int (has type of)
-- Int :: *
-- Cons 1 Nil :: List Int
-- List Int :: *
-- List :: * -> * -- mintha várna egy paramot, és utána adná meg a típust
-- Ahhoz hogy vmi funktor legyen, kell h várjon 1 paramot

-- a functornak kategóriaelméletből jövő alapjai vannak ,és nem csak konténerken megy,
-- hanem kontextusokat lehet generálni, és ott, pl IO

-- funktor: 1 műveletet vár el:
-- Functor (f :: * -> *)
--   fmap :: (a -> b) -> f a -> f b

class Functor (f :: * -> *) where
  fmap :: (a -> b) -> f a -> f b

instance Functor List where
  fmap :: (a -> b) -> List a -> List b
  fmap f Nil = Nil -- elsőnek "List a" a típusa, másodiknak "List b" !
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)

-- fmap (*2) Nil == Nil
-- fmap (*2) (Cons 1 (Cons 2 Nil)) == (Cons 2 (Cons 4 Nil))

-- fmap id (Cons 1 (Cons 2 Nil)) == (Cons 1 (Cons 2 Nil))
-- fmap ((*2).(+1)) (Cons 1 (Cons 2 Nil)) == (fmap (*2) . fmap(+1) (Cons 1 (Cons 2 Nil)))
