module Practice7 where

import Control.Monad.State

type CMap a = [(a, Int)]
type CounterM a = State (CMap a)

inc :: Eq a => a -> CMap a -> CMap a
inc x [] = [(x, 1)]
inc a ((y, n):ys)
  | a == y = (y, (+) n 1):(inc a ys)
  | otherwise = (y, n):(inc a ys)

-- incM :: Eq a => a -> CounterM a ()

-- Use inc
incM :: Eq a => a -> CounterM a ()
incM x = do
  cmap <- get
  let cmap' = inc x cmap
  put cmap'

-- Use incM
countM :: Eq a => [a] -> CounterM a ()
countM [] = pure ()
countM (x:xs) = do
  incM x
  countM xs

-- use countM
-- count :: Eq a => a -> [a] -> Maybe Int
-- count x xs = lookup x occurrences where
--   execState (countM xs) []
