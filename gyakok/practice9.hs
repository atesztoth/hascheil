{-# LANGUAGE InstanceSigs #-}

module Practice9 where

import Control.Applicative

newtype Parser a
  = Parser { runParser :: String -> Maybe (a, String) }

instance Functor Parser where
  fmap :: (a -> b) -> Parser a -> Parser b
  fmap f (Parser g) = Parser $ \str -> case g str of
    Just (x, str') -> Just (f x, str')
    Nothing        -> Nothing

instance Applicative Parser where
  pure :: a -> Parser a
  pure x = Parser $ \str -> pure (x, str)

  (<*>) :: Parser (a -> b) -> Parser a -> Parser b
  (<*>) (Parser pF) (Parser g) = Parser $ \str -> do
    (f, str' ) <- pF str
    (x, str'') <- g str'
    pure (f x, str)

eof :: Parser ()
eof = Parser $ \str -> case str of
  "" -> Just ((), str)
  _  -> Nothing

char :: Char -> Parser Char
char c = Parser $ \str -> case str of
  (x:xs) | x == c -> Just (c, xs)
  _               -> Nothing


instance Alternative Parser where
  empty :: Parser a
  empty = Parser $ \_ -> Nothing

  (<|>) :: Parser a -> Parser a -> Parser a
  (<|>) p q = Parser $ \str -> case runParser p str of
    Just res -> Just res
    Nothing  -> runParser q str -- Nooothing el maatteeeeeeers

charXY :: Parser Char
charXY = char 'x' <|> char 'y'

anyChar :: Parser Char
anyChar = foldr (<|>) empty $ map char ['a'..'z']

string :: String -> Parser [Char]
string = traverse char

-- *Practice09> runParser (string "asd") "asdqwe"
-- Just ("asd","asdqwe")

some' :: Alternative f => f a -> f [a]
some' f = (:) <$> f <*> many' f

many' :: Alternative f => f a -> f [a]
many' f = some' f <|> pure []

-- *Practice09> runParser (many' (char 'a')) "a"
-- Just ("a","a")
-- *Practice09> runParser (some' (char 'a')) "a"
-- Just ("a","a")

data Bit = F | T
    deriving (Eq, Ord, Show)

data ShortByte = SB Bit Bit Bit Bit
    deriving (Show)

bitF :: Parser Bit
bitF = char '0' *> pure F

bitT :: Parser Bit
bitT = char '1' *> pure T

bit :: Parser Bit
bit = bitF <|> bitT

shortByte :: Parser ShortByte
shortByte = SB <$> bit <*> bit <*> bit <*> bit

instance Monad Parser where
  return :: a -> Parser a
  return = pure

  -- mert aki a-t parsol, parsoljon b-t is!
  (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  (>>=) p k = Parser $ \str -> case runParser p str of
    Nothing        -> Nothing
    Just (x, str') -> runParser (k x) str'

times :: Int -> Parser a -> Parser [a]
-- times 0 p = pure []
-- times n p = (:) <$> p <*> times (n-1) p
times n p = traverse (\_ -> p) [1..n]

digit :: Parser Int
digit = fmap (\n -> n - 48)
        . fmap fromEnum
        . foldr (<|>) empty
        $ map char ['0'..'9']

int :: Parser Int
int = undefined

foo :: Parser a -> Parser [a] -- fffuuuu
foo p = do
  n <- int
  times n p
