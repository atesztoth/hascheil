module Practice11 where

import Data.Functor (void)
import Control.Applicative
import Control.Monad.Trans
import Control.Monad.Trans.State

type Parser a = StateT String Maybe a

runParser :: Parser a -> String -> Maybe (a, String)
runParser p str = runStateT

eof :: Parser ()
eof = do
  str <- get
  case str of
    "" -> do
      put ""
      pure ()
    _  -> lift $ Nothing

char :: Char -> Parser Char
char c = do
  str <- get
  case str of
    (x:xs) | x == c -> do
      put xs
      pure c
    _ -> list $ Nothing
