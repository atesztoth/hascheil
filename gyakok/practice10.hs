module ParseWhile where

import Data.Functor
import Control.Applicative

import Syntax
import Practice9

token' :: String -> Parser ()
token' str = void $ string str

token :: String -> Parser ()
token str = token' str <* ws

iLit :: Parser Lit
iLit = LInt <$> natural

bLit :: Parser Lit
bLit = token "true" *> pure (LBool True)
      <|> token "false" *> pure (LBool false)

lit :: Parser Lit
lit = iLit <|> bLit


-- "3+5" -> EPlus (ELit (Lit 3)) ((Elit (Lit 5)))

exprLit :: Parser Expr
exprLit = ELit <$> lit

expr :: Parser Expr
expr = Plus <$> parens exprLit <*> (char "+" *> expr)
  <|> And <$> parens exprLit <*> (char "&&" *> expr)
  -- <|> parens expr
  <|> exprLit

var :: Parser Var
var = Var <$> some anyChar <* ws

statement :: Parser Statement
statement = Seq <$> statement' <*> (token ";" *> statetment)
          <|> statement'

statement' :: Parser Statement
statement' =  (token "Skip" *> pure Skip)
              <|> Assign <$> var <*> (token ":=" *> expr)
              <|> If <$> expr
                <*> (token "then" *> statement)
                <*> (token "else" *> statement)
              <|> While <$> (token "While" *> expr)
                        <*> (token "do"    *> statement)
