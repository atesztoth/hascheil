{-# LANGUAGE KindSignatures, InstanceSigs #-}

module Practice4 where

nameIdDb :: [(String, Int)]
nameIdDb = [("John", 1),("Richard", 2),("Julie", 3)]

-- lookup' :: Eq k => a -> [(k,v)] -> Maybe v
-- lookup' key [] = Nothing
-- lookup' key ((currKey, currVal) : xs)
--         | key == currKey = Just currVal
--         | otherwise lookup' xs

data Currency = USD | GBP
  deriving (Eq, Ord, Show)

idMoneyDb :: [(Int, (Double, Currency))]
idMoneyDb = [
  (1, (11, USD)),
  (5, (22, USD)),
  (3, (33, GBP))
  ]

exchangeRateDB :: [(Currency, Double)]
exchangeRateDB = [ (USD, 1), (GBP, 1.1) ]

getMoneyInUSD :: String -> Maybe Double
getMoneyInUSD name = case lookup name nameIdDb of
  Nothing -> Nothing
  Just id -> case lookup id idMoneyDb of
    Nothing -> Nothing
    Just (amount, currency) -> case lookup currency exchangeRateDB of
      Nothing -> Nothing
      Just exRate -> Just $ amount * exRate -- azért ha lenne még vagy 4 ilyen lookup, már már amolyan callback hellben érezném magam JS-ből

-- Na itt jön képbe a Monad. Hibakontextust mond kb. Check this out:

-- monad, hogy tudjuk miért hasalt el vmi
getMoneyInUSD' :: String -> Maybe Double
getMoneyInUSD' name = do
  id <- lookup name nameIdDb
  (amount, currency) <- lookup id idMoneyDb
  exRate <- lookup currency exchangeRateDB
  asd <- pure "asd" -- beemelgetek itt nagyba
  pure $ amount * exRate -- kontextuson kívüli érték, ez ugye nem nagyon fog elhalni Nothingal... :D szóval ez pure.
  -- pure beceneve a return is, de hívjuk csak purenak amikor csak lehet. Imperatív sztájl.
  -- ha returnnak hívjuk akkor sem visszaad, csak végez egy számítást!
  -- beemeli az értéket a contextusba!

class Monad' (m :: * -> *) where
  return :: a -> m a -- returnnek hívjuk
  (>>=) :: m a -> (a -> m b) -> m b -- bindnak hívjuk | itt a második cucc az egy continuationt mond meg az első "számítás" (m a) függvényében!
  -- control flow-t lehet vele variálni!

-- konkrétan a maybenél
-- + minden ami monád, az applicatív,
-- + minden ami applikatív, az funktor is!

instance Monad' Maybe where
  return :: a -> Maybe a
  return x = Just x

  (>>=) :: Maybe a -> (a -> Maybe b) -> Maybe b
  (>>=) m cont = case m of
    Nothing -> Nothing
    Just x -> cont x

-- Desugaring vhogy így néz ki:
--     id <- lookup name nameIdDb
--     (amount, currency) <- lookup id idMoneyDb
--     exRate <- lookup currency exchangeRateDB
--     pure $ amount * exRate

-- lookup name nameIdDb >>= (\id ->
--   lookup id idMoneyDb >>= (\(amount, currency) ->
--     lookup currency exchangeRateDB >>= (\exRate ->
--       pure $ amount * exRate )))
