-- Típusosztályok

-- egyenlőségvizsgálat
-- először primitívekre
eqBool :: Bool -> Bool -> Bool
eqBool True True = True
eqBool False False = True
eqBool _ _ = False

-- típusosztályok nélkül listára
eqList :: (a -> a -> Bool) -> [a] -> [a] -> Bool
eqList eqa [] [] = True
eqList eqa (x:xs) (y:ys) = eqa x y && eqList eqa xs ys
eqList _ _ _ = False

-- eq függvények összerakása teljesen mechanikus
---------------------------------
-- típusosztály: kódgenerálás típusok struktúrája szerint

class Eq' a where         -- osztály deklaráció
    eq :: a -> a -> Bool  -- osztály metódus

instance Eq' Bool where -- instance
    -- ez lesz a típus ugye mivel helyettesít eq :: Bool -> Bool -> Bool
    -- eq = eqBool
    eq True True = True
    eq False False = True
    eq _ _ = False

-- feltételes: csak akkor van eq listára, ha van az elemekre is
-- a fat arrow neve "class constraint", csak nem mondták ily' szépen el
-- tehát itt pl azt mondja, hogy az a typenak muszáj az Eq' osztály elemének lennie, és folytatódik a def...
instance Eq' a => Eq' [a] where
    eq [] [] = True
    eq (x:xs) (y:ys) = eq x y && eq xs ys
               --      Eq' a     Eq' [a]
    eq _ _ = False
    -- eq eqList eq

-- példa: eq True False == False
--        eq True True == True
--        eq True [] -- hiba ha nincs Eq [a] instance

-- párokra:
instance (Eq' a, Eq' b) => Eq' (a, b) where
    eq (a, b) (a', b') = eq a a' && eq b b'

-- standard megfelelő:
-- Eq osztály
-- metódus: (==) operátor

-- kérdés: Milyen típusra nincs Eq instance?
-- pl nagyon nem hatékony: Int -> Int
-- egyáltalán nincs: [Bool] -> Int
-- mert [Bool]-nak végtelen sok értéke van

-- GHCI kitérő
------------------------------------------
-- :r                   újratöltés
-- :l <file>            fájl betöltés
-- :t <kifejezés>       kifejezés típusa
-- :i <név>             információ névről
-- jó pl operátorokról való információk keresésére
-- osztály instancejainkat listázása: szintén :i

-- !!! type hole !!!
-- bármilyen kifejezésben _-t hasnzálunk, akkor
-- utána GHC üzenetben megkapjuk azt a típust, melyet az underscore helyére
-- kelle írni.

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
-- map' f (x:xs) = _
map' f (x:xs) = f x : map' f xs
-- 1. lépés   = _
-- 2.         = f x : _
-- 3.         = f x : map' f xs

f1 :: (b -> c) -> (a -> b) -> a -> c
f1 = \f g a -> f (g a)
    -- = 
    -- = \f g a ->
    -- = \f g a -> f _
    -- = \f g a -> f (g _)
    -- = \f g a -> f (g a)

-- Ismétlés: rekurzív ADT-k
data List a = Empty | Const a (List a)

-- Olyan bináris fák típusa, melyen semmi extra info nincs:
-- bináris fák
--  levél
--  bináris elágazás

-- data BinTree = Leaf | Node BinTree BinTree

-- t1 :: BinTree -- azaz a t1 típusa a BinTree
-- t1 = Leaf
-- t2 = Node Leaf Leaf
-- t3 = Node t1 t2
-- t4 = Node (Node Leaf Leaf) BinTree
-- t5 = Node t4 t4
-- t6 = Node t6 t6

-- Int-tel annotált Bináris fa
data Tree a = Leaf | Node a (Tree a) (Tree a)
        deriving Show -- olyan instancet generál le automatikusan, melyből stinget lehet csin.

-- végtelen bináris fa, melyben rétegenként egyre nagyobb szám van (Int címke)
tree :: Tree Int
tree = go 0 where
    -- (Haskell konvenció: lokális segédfv. neve gyakran go)
    -- go :: Int -> Tree Int
    go n = Node n (go (n + 1)) (go (n + 1))

-- go 0 = Node 0 (go 1) (go 1)
-- go 1 = Node 1 (go 2) (go 2)
-- go 2 = Node 1 (go 3) (go 3)

-- hasonló mint a listekre a take
takeTree :: Int -> Tree a -> Tree a
takeTree 0 t                = Leaf
takeTree n Leaf             = Leaf
takeTree n (Node a t1 t2)   = Node a (takeTree (n - 1) t1) (takeTree (n - 1) t2)

-- *Test> takeTree 2 tree
-- Node 0 (Node 1 Leaf Leaf) (Node 1 Leaf Leaf)

-- feladat:
-- írjunk olyan instancekat...
-- instance Eq a => Eq (Tree a)

instance Eq a => Eq (Tree a) where
     (Node x xl xr) == (Node y yl yr) = (x == y) && xl == yl