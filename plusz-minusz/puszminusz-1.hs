newtype E a = E { appE :: a -> a }

instance Semigroup (E a) where
    (<>) (E n) (E k) = n$k

-- appE (E (*2) <> E (+1)) 5 == 12
-- appE (E tail <> E tail) [1,2,3] == [3]
-- appE (E (drop 4) <> E (take 5)) [1..] == [5]