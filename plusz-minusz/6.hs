clean :: a -> (x -> a)
clean x = (\_ -> x)

tie :: (x -> a) -> (a -> x -> b) -> x -> b
tie f g x = g (f x) x
