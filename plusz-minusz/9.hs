module Plusminus09 where

import Data.Set
import Control.Monad.Writer

uniquesM :: Ord a => [a] -> Writer (Set a) Int
uniquesM [] = writer (0, mempty)
uniquesM (x:xs) = do
  (n, rem) <- uniquesM xs
  pure (n + x, rem <> xs)

