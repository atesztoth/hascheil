-- A Swap egy wrapper típus, melynek célja, hogy specializálja a Semigroup típusosztály viselkedését egy adott típusra.
-- Feladat 1
-- Példányosítsuk a Semigroup típusosztályt a Swap m típusra, ahol tudjuk m-ről, hogy létezik már Semigroup példánya. 
-- A Swap m-re értelmezett (<>) művelet viselkedése abban térjen el az m-re értelmezettől, hogy megcseréli a függvény argumentumait.

instance Semigroup a => Swap a where
    getSwap 

-- Feladat 2
-- Példányosítsuk a Monoid típusosztályt a Swap m típusra, ahol tudjuk m-ről, hogy létezik már Monoid példánya. 
-- A Swap m típusú mempty érték abban különbözzön az m típusútól, hogy alkalmazva van rá a Swap adatkonstruktor.
