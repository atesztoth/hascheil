-- plusz minusz
twiceM :: Monad m => m a -> m (a, a)
twiceM m = do
  x <- m
  y <- m
  pure (x, y)

compM :: Monad m => (b -> m c) -> (a -> m b) -> a -> m c
compM f g x = do
  r <- g x
  r2 <- f r
  pure r2
