import Control.Monad.Writer
import Data.Set

uniquesM :: Ord a => [a] -> Writer (Set a) Int
uniquesM as =
  let s = fromList as
  in tell s >> pure (size s)


a <- ma
b
-- =
ma >>= \a ->
b

ma
mb
-- =
ma >>= \_ ->
mb


