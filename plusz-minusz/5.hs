{-# LANGUAGE InstanceSigs #-}

data NatF a = ZeroF | SucF a
  deriving (Eq, Ord, Show)

data NonEmpty a = NonEmpty a [a]
  deriving (Eq, Ord, Show)

-- elso
instance Functor NatF where
  -- fmap :: (a -> b) -> NatF a -> NatF b
  fmap f ZeroF = ZeroF
  fmap f (SucF a) = SucF (f a)

instance Functor NonEmpty where
  -- fmap :: (a -> b) -> NonEmpty a -> NonEmpty b
  fmap f (NonEmpty a []) = NonEmpty (f a) []
  fmap f (NonEmpty a xs) = NonEmpty (f a) ((fmap f) xs)
