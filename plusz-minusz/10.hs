import Control.Applicative

newtype Parser a
  = P { runParser :: String -> Maybe (a, String) }


instance Applicative Parser where
  pure :: a -> Parser a
  pure x = P $ \str -> Just (x, str)

  (<*>) :: Parser (a -> b) -> Parser a -> Parser b
  (<*>) (P pF) (P g) = P $ \str -> do
    (f, str')  <- pF str
    (x, str'') <- g str'
    pure (f x, str'')

instance Alternative Parser where
  empty :: Parser a
  empty = P (\_ -> Nothing)

  (<|>) :: Parser a -> Parser a -> Parser a
  (<|>) p q = P $ \str -> case runParser p str of
    Just res -> Just res
    Nothing  -> runParser q str

char :: Char -> Parser Char
char c = P $ \str -> case str of
  (x:xs) | x == c -> Just (c, xs)
  _               -> Nothing

digit :: Parser Int
digit = fmap (\n -> n - 48)
      . fmap fromEnum
      . foldr (<|>) empty
      $ map char ['0'..'9']

intTuple :: Parser (Int, Int)
intTuple = (,) <$> digit <*> digit

bits :: Parser [Int]
bits = undefined
