module Beugro7 where

import Control.Monad.State

lengthM :: [a] -> State Int ()
lengthM [] = pure ()
lengthM (x:xs) = do
  x <- get
  put (1 + x)

reduce :: Monad m => m (m a) -> m a
reduce x = _ -- fail :(
