
-- State, Reader
--------------------------------------------------------------------------------

import Control.Monad.State
import Control.Monad.Reader
import Data.Maybe


-- Ismétlés: kifejezés kiértékelése (lásd Notes03.hs)
------------------------------------------------------------

data Expr = Literal Int | Add Expr Expr | Mul Expr Expr | Var String
  deriving Show

-- "10 + (20 + x)"
e1 = Add (Literal 10) (Add (Literal 20) (Var "x"))

-- "y * x + 100"
e2 = Add (Mul (Var "y") (Var "x")) (Literal 100)

-- Írj kiértékelő függvényt Expr-hez. Paraméterként megkapunk egy '[(String,
-- Int)]' listát, ez minden változónévhez megad egy értéket.

-- példa:
-- eval [("x", 10)] e1 == 40
-- eval [("x", 2), ("y", 2)] e2 == 400

-- példa:
-- eval [("x", 10)] e1 == 40
-- eval [("x", 2), ("y", 2)] e2 == 400

evalExpr :: [(String, Int)] -> Expr -> Int
evalExpr valList (Literal x) = x
evalExpr valList (Add l r) = (+) (evalExpr valList l) (evalExpr valList r)
evalExpr valList (Mul l r) = (*) (evalExpr valList l) (evalExpr valList r)
evalExpr valList (Var s) = case lookup s valList of
  Just a -> a
  Nothing -> undefined


-- Egészítsük ki a nyelvet értékadással a következőképpen:
type Program = [(String, Expr)]

-- Egy program értékadó utasítások listája. pl:
p1 = [("x", Literal 10), ("y", Literal 20), ("x", Add (Var "x") (Var "y"))]

-- ez megfelel annak hogy: x := 10; y := 20; x := x + y


-- Írj kiértékelést. Kezdetben a változókörnyezet legyen üres. Ha egy új
-- változóhoz rendel a program értéket, egészítsd ki a környezetet az új
-- változóval és értékkel, egyébként módosítsd meglévő változó értékét.
-- Legyen a "runProgram" visszatérési értéke a környezet végső állapota.

runProgram :: Program -> [(String, Int)]
runProgram p = execState (evalProgram p) []

evalProgram :: Program -> State [(String, Int)] ()
evalProgram [] = pure ()
evalProgram ((s, e):xs) = do
  tmp <- get
  let v = evalExpr tmp e
  put ((s, v):tmp)
  evalProgram xs

-- Példák:
-- runProgram [("x", Literal 10)] == [("x", Literal 10)]
-- runProgram [] == []
-- runProgram [("x", Literal 10), ("x", Var "x")] == [("x", 10)]
-- runProgram [("x", Literal 10), ("x", Add (Var "x") (Var "x"))] == [("x", 20)]
-- runProgram [("x", Literal 10), ("y", Var "x")] == [("x", 10), ("y", 10)]


-- Kiértékelés Maybe-vel
--------------------------------------------------------------------------------

-- Értékeld ki a következő kifejezéseket úgy, hogy az eredmény Nothing legyen,
-- ha egy változó nincs a környezetben. Használd a Maybe monádot.

data Exp2 = Var2 String | Add2 Exp2 Exp2 | Mul2 Exp2 Exp2 | Literal2 Int
  deriving Show

teste2 = Add2 (Literal2 20) (Literal2 30)

evalExpr' :: [(String, Int)] -> Exp2 -> Maybe Int
evalExpr' varList (Literal2 x) = pure x
evalExpr' varList (Add2 l r) = liftM2 (+) (evalExpr' varList l) (evalExpr' varList r)
evalExpr' varList (Mul2 l r) = liftM2 (*) (evalExpr' varList l) (evalExpr' varList r)
evalExpr' varList (Var2 s) = lookup s varList


-- Kiértékelés Reader-el
--------------------------------------------------------------------------------


-- Értékeld ki a következő kifejezéseket, amelyekben let-definíciók is vannak,
-- Reader monád segítségével.

data Exp3 = Var3 String | Add3 Exp3 Exp3 | Mul3 Exp3 Exp3 | Literal3 Int
          | Let String Exp3 Exp3 -- let x = exp1 in exp2
  deriving Show

-- A változókörnyezetet a Reader-ben tároljuk. Használd a "local" függvényt
-- a Let-ek kirétékelésére (a környezet lokális kiegészítéséhez).
evalExp3 :: Exp3 -> Reader [(String, Int)] Int
evalExp3 (Literal3 x) = pure x
evalExp3 (Var3 s) = do
  val <- asks(lookup s)
  pure (fromJust val)
evalExp3 (Add3 e1 e2) = liftM2 (+) (evalExp3 e1) (evalExp3 e2)
evalExp3 (Mul3 e1 e2) = liftM2 (*) (evalExp3 e1) (evalExp3 e2)
evalExp3 (Let s l r)  = do
    valL <- evalExp3 l
    valR <- local (\env -> (s, valL):env) (evalExp3 r)
    return valR
